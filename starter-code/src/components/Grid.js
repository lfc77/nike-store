import React, { Component } from "react";

import "./Grid.css";
import Filters from "./Filters";
import Content from "./Content";
//{/*this.props.children*/}
export default class Grid extends Component {
  render() {

    const shoesCollection = this.props.shoes;

    return (
      <div className="grid">
        <Filters></Filters>
        <Content shoesCollection = {shoesCollection}></Content>
      </div>
    );
  }
}
