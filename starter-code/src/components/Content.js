import React, { Component } from "react";

import "./Content.css";
import Gallery from "./Gallery";

export default class Content extends Component {

  render() {

    const shoesCollection = this.props.shoesCollection;

    return (
       <div className="Content">
          <h3 className="Content-Title">
            Men’s Shoes & Skeakers
            <span className="Results">{shoesCollection.length}</span>
          </h3>
          <p className="Results-Description">Explore the latest shoes for men for every sport, workout and everyday look. Built for ultimate performance and sneaker style, Nike shoes for men deliver cutting-edge technologies specific to your sport in iconic designs.</p>
          <Gallery shoesCollection = {shoesCollection}></Gallery>
        </div>
    );
  }
}
