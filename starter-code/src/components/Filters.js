import React, { Component } from "react";

import "./Filters.css";

export default class Filters extends Component {
  constructor(){
    super();
    

    this.filter1 = React.createRef();
    this.filter2 = React.createRef();
    this.filter3 = React.createRef();
    /*this.state = {
      filters : {
          filter1 : false,
          filter2 : false,
          filter3 : false,
          filter4 : false,
          filter5 : false
      }
    }*/
  }

  getFilters = (e) => {
      const numberFilter = e.target.id;
      
        /*
          filter1 : numberFilter === 1 ? true : false,
          filter2 : numberFilter === 2 ? true : false,
          filter3 : numberFilter === 3 ? true : false
        */
      if(numberFilter >= 1 && numberFilter <= 3){
        console.log("Entro al if ",numberFilter);
        
        this.filter2.current.textContent = "Hola";
        this.filter2.current.defaultChecked = "true";

        /*this.setState({
        
         filters :{
          filter1 : true,
          filter2 : false,
          filter3 : false
         }

        })*/
      }
      console.log(this.filter2.current.value);
  };

  render() {
    
    return (

      <aside className = "Section">
        <h3 className = "Section-Title">Filters</h3>
        <div className = "Block">
          <h4 className = "Block-Title">Shop By Price</h4>
          <label className = "Block-Option">
            <input ref = {this.filter1} id = "1" type = "checkbox" onChange = {this.getFilters} defaultChecked = {false}/> Under $50
          </label>
          <label className = "Block-Option">
            <input ref = {this.filter2} id = "2" type="checkbox" defaultChecked = {false} value ="ho"/> $50 to $150
          </label>
          <label className = "Block-Option">
            <input id = "3" type="checkbox" defaultChecked = {false}/> Up to $150
          </label>
        </div>

        <div className = "Block">
          <h4 className = "Block-Title">Sport</h4>
          <label className = "Block-Option">
            <input type="checkbox" /> Lifestyle
          </label>
          <label className = "Block-Option">
            <input type="checkbox" /> Running
          </label>

          <label className = "Block-Option">
            <input type="checkbox" /> Basketball
          </label>

          <label className = "Block-Option">
            <input type="checkbox" /> Soccer
          </label>

          <label className = "Block-Option">
            <input type="checkbox" /> Training & Gym
          </label>
        </div>

        <div className = "Block">
          <h4 className = "Block-Title">Closure Type</h4>
          <label className = "Block-Option">
            <input type="checkbox" /> Click
          </label>

          <label className = "Block-Option">
            <input type="checkbox" /> Slip
          </label>

           <label className = "Block-Option">
            <input type="checkbox" /> On Strap
          </label>
        </div>

        <div className ="Block">
          <h4 className ="Block-Title">Color</h4>
          <button className ="Block-Color" data-color="white"></button>
          <button className ="Block-Color" data-color="silver"></button>
          <button className ="Block-Color" data-color="yellow"></button>
          <button className ="Block-Color" data-color="gold"></button>
          <button className ="Block-Color" data-color="orange"></button>
          <button className ="Block-Color" data-color="green"></button>
          <button className ="Block-Color" data-color="blue"></button>
          <button className ="Block-Color" data-color="pink"></button>
          <button className ="Block-Color" data-color="olive"></button>
          <button className ="Block-Color" data-color="red"></button>
          <button className ="Block-Color" data-color="purple"></button>
          <button className ="Block-Color" data-color="grey"></button>
        </div>

        <div className="Block">
          <h4 className="Block-Title">Size</h4>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            <button className="Block-Size">3</button>
            <button className="Block-Size">3.5</button>
            <button className="Block-Size">4</button>
            <button className="Block-Size">4.5</button>
            <button className="Block-Size">5</button>
            <button className="Block-Size">5.5</button>
            <button className="Block-Size">6</button>
            <button className="Block-Size">6.5</button>
            <button className="Block-Size">7</button>
            <button className="Block-Size">7.5</button>
            <button className="Block-Size">8</button>
            <button className="Block-Size">8.5</button>
            <button className="Block-Size">9</button>
            <button className="Block-Size">9.5</button>
            <button className="Block-Size">10</button>
            <button className="Block-Size">10.5</button>
            <button className="Block-Size">11</button>
            <button className="Block-Size">11.5</button>
            <button className="Block-Size">12</button>
            <button className="Block-Size">12.5</button>
            <button className="Block-Size">13</button>
            <button className="Block-Size">13.5</button>
            <button className="Block-Size">14</button>
            <button className="Block-Size">14.5</button>
            <button className="Block-Size">15</button>
            <button className="Block-Size">15.5</button>
            <button className="Block-Size">16</button>
            <button className="Block-Size">16.5</button>
            <button className="Block-Size">17</button>
            <button className="Block-Size">18</button>
            <button className="Block-Size">19</button>
            <button className="Block-Size">20</button>
            <button className="Block-Size">21</button>
            <button className="Block-Size">22</button>
          </div>
        </div>
      </aside>
      
    );
  }
}
