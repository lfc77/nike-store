import React, { Component } from "react";

import "./Gallery.css";

export default class Gallery extends Component {
  render() {
    const shoesCollection = this.props.shoesCollection;

    return (
      
        <div className="Gallery">
          {shoesCollection.map(shoe => {

            return  <div key = {shoe._id}  className="Shoe" >
                      <img src = {shoe.url}/>
                      {shoe.colors.map(color => {
                        return <span key = {color} className="Shoe-Color">{color} </span>
                      })}
                      <hr/>
                      <em className="Shoe-Title">{shoe.name}</em>
                      <span className="Shoe-Type">{shoe.type}</span>
                      <span className="Shoe-Price">${shoe.price}</span>
                    </div>
          })}
        </div>
    );
  }
}
