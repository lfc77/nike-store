import React, { Component } from 'react';
import Header from './components/Header';
import Grid from './components/Grid';
import axios from 'axios';

class App extends Component {

  constructor(){
    super();

    this.state = {
        shoes: []
    };
  }
  
  getUsers  = () => {
    axios.get('https://nikeapi.herokuapp.com/api/v1/shoes')
    .then(response => response.data)
    .then(data => this.setState({ shoes: data.data}))
    .catch(function (error) {
        console.log(error);
    });
  };

  componentDidMount = () => {
    this.getUsers();
  };

  render() {

    return (
      <div>
        <Header></Header>
        <Grid shoes = {this.state.shoes}></Grid>
      </div>
    );
  }
}

export default App;
